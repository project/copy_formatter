(function () {
  // Get the copy button element
  var copyButtons = document.querySelectorAll('.copy-formatter');

  // Add a click event listener to the copy button
  copyButtons.forEach(function (copyButton) {
    copyButton.addEventListener('click', function () {
      var textToCopy = copyButton.getAttribute('data-content');
      // Try using the Clipboard API
      if (navigator.clipboard && navigator.clipboard.writeText) {
        navigator.clipboard.writeText(textToCopy).then(function () {
          // Change the button text to "Copied"
          copyButton.value = 'Copied!';
          // Enable the button after 10 seconds
          setTimeout(function () {
            copyButton.value = 'Copy';
          }, 10000);
        });
      }
      // Fallback for browsers that don't support the Clipboard API
      else {
        // Create a hidden input element
        var hiddenInput = document.createElement('input');
        hiddenInput.value = textToCopy;
        document.body.appendChild(hiddenInput);
        // Select and copy the text from the hidden input
        hiddenInput.select();
        document.execCommand('copy');
        // Remove the hidden input from the DOM
        document.body.removeChild(hiddenInput);
        // Change the button text to "Copied"
        copyButton.value = 'Copied!';
        // Enable the button after 10 seconds
        setTimeout(function () {
          copyButton.value = 'Copy';
        }, 10000);
      }
    });
  });
})();
