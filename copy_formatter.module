<?php

/**
 * @file
 * Hooks implementation for the Copy Formatter module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function copy_formatter_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.copy_formatter':
      return '<p>' . t('The Copy Formatter module allows you to add a copy button to media reference fields, making it easy for users to copy the media embed code.') . '</p>
      <h3>' . t('Introduction') . '</h3>
      <p>' . t('The Copy Formatter module provides a field formatter for media reference fields, which displays a copy button.') . '</p>
      <h3>' . t('Usage') . '</h3>
      <ol>
        <li>' . t('Enable the Copy Formatter module.') . '</li>
        <li>' . t('Go to the "Manage Display" page of the content type or entity bundle where you have media reference fields') . '</li>
        <li>' . t('Choose the "Copy" formatter for the media reference field.') . '</li>
        <li>' . t('Save the display settings.') . '</li>
        <li>' . t('On the rendered content, you will see a copy button.') . '</li>
        <li>' . t('Click the copy button to copy the media embed code to the clipboard.') . '</li>
      </ol>';

    default:
      return '';
  }
}
