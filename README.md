# Copy Formatter

Copy Formatter module provides a copy field formatter for media referenced
fields in Drupal 9/10. It allows users to copy the embedded media code with a
single click and use it in other parts of their website or external
applications. The module supports various media types, including images,
videos, audios, PDFs, and remote videos.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/copy_formatter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/copy_formatter).

## Table of contents

- Requirements
- Installation
- Configuration

## Requirements

This module requires media module.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

- After enabling the module, navigate to the "Manage Display" page of the
content type or entity bundle where you have media reference fields.
- Select the "Copy" formatter for the desired media referenced fields.
- Save the display settings.
- When viewing a media entity with the selected formatter, users will see a
copy button and on clicking the copy button, the embed code for that media
will get copied.
- Users can copy the embed code and use it in other parts of the website or
external applications.
