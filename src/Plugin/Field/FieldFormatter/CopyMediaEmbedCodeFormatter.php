<?php

namespace Drupal\copy_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of 'copy_media_embed_code_formatter' field formatter.
 *
 * @FieldFormatter(
 *   id = "copy_media_embed_code_formatter",
 *   label = @Translation("Copy"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class CopyMediaEmbedCodeFormatter extends FormatterBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a new CopyMediaEmbedCodeFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\File\FileSystemInterface $file_url_generator
   *   The file URL generator service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, RendererInterface $renderer, FileSystemInterface $file_url_generator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->renderer = $renderer;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('renderer'),
      $container->get('file_system')
    );
  }
  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $target_entity_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    return $target_entity_type === 'media';
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $entity = $item->entity;
      if ($entity instanceof Media) {
        $embed_code = '';
        $media_type = $entity->bundle();
        if ($media_type === 'image') {
          $file = $entity->field_media_image->entity;
          if ($file) {
            $path = $file->getFileUri();
            $image_path = $this->fileUrlGenerator->realpath($path);
            $alt_text = $file->alt ?: '';
            $embed_code = '<img src="' . $image_path . '" alt="' . $alt_text . '">';
          }
        }
        elseif ($media_type === 'video') {
          $file = $entity->field_media_video_file->entity;
          if ($file) {
            $path = $file->getFileUri();
            $video_path = $this->fileUrlGenerator->realpath($path);
            $embed_code = '<video src="' . $video_path . '"></video>';
          }
        }
        elseif ($media_type === 'audio') {
          $file = $entity->field_media_audio_file->entity;
          if ($file) {
            $path = $file->getFileUri();
            $audio_path = $this->fileUrlGenerator->realpath($path);
            $embed_code = '<audio src="' . $audio_path . '"></audio>';
          }
        }
        elseif ($media_type === 'pdf') {
          $file = $entity->field_media_document->entity;
          if ($file) {
            $path = $file->getFileUri();
            $doc_path = $this->fileUrlGenerator->realpath($path);
            $embed_code = '<iframe src="' . $doc_path . '"></iframe>';
          }
        }
        elseif ($media_type === 'remote_video') {
          $remote_video_url = $entity->field_media_oembed_video->value;
          if (!empty($remote_video_url)) {
            $embed_code = '<iframe src="' . $remote_video_url . '"></iframe>';
          }
        }
        $elements[$delta] = [
          '#type' => 'button',
          '#value' => $this->t('Copy'),
          '#attached' => [
            'library' => ['copy_formatter/copy_formatter'],
          ],
          '#attributes' => [
            'class' => ['copy-formatter'],
            'data-content' => $embed_code,
          ],
        ];
      }
      else {
        $elements[$delta] = $item->view();
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    return $element;
  }

}
